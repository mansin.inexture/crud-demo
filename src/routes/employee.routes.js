const express = require('express')
const router = express.Router()
const employeeController = require('../controllers/employee.controller');

// Retrieve all employees
router.get('/', employeeController.findAll);

// // Create a new employee
router.get('/create', employeeController.create);
router.post('/save', employeeController.save);

// // Retrieve a single employee with id
router.get('/edit/:id', employeeController.findById);

// // Update a employee with id
router.post('/update/:id', employeeController.update);

// // Delete a employee with id
router.post('/delete/:id', employeeController.delete);

module.exports = router;