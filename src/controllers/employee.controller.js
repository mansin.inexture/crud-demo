"use strict";

const Employee = require("../models/employee.model");
  exports.findAll = (req, res) => {
    Employee.find()
      .then((employees) => {
        // console.log('ein ===', window.location.path);
        // res.status(200).json((employees)); // This runs as well.
        res.render('employees/show', { employeeList: employees })
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving employees.",
        });
      });
  };

exports.create = function (req, res) {
  res.render('employees/create');
}

exports.save = function (req, res) {
  const new_employee = new Employee(req.body);
  if(!req.body){
    res.status(400).send({ message : "Content can not be emtpy!"});
    return;
  } else {
    new_employee
      .save()
      .then((data) => {
        // res.send(data);
        res.redirect('/employees');
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the employee.",
        });
      });
  }
};

exports.findById = function (req, res) {
  // Employee.findById(req.params.id, function(err, employee) {
  //   console.log('employee', employee, req.params);
  //     if (err)
  //     res.send(err);
  //     res.json(employee);
  // });

  Employee.findById(req.params.id)
    .then((employee) => {
      if (!employee) {
        return res.status(404).send({
          message: "employee not found with id " + req.params.id,
        });
      }
      res.render("employees/edit", {employee: employee});
      // res.send(employee);
    })
    .catch((err) => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "employee not found with id " + req.params.id,
        });
      }
      return res.status(500).send({
        message: "Error retrieving employee with id " + req.params.id,
      });
    });
};

exports.update = function (req, res) {
  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    res
      .status(400)
      .send({ error: true, message: "Please provide all required field" });
  } else {
    Employee.findByIdAndUpdate(req.params.id, req.body, { new: true })
      .then((employee) => {
        if (!employee) {
          return res.status(404).send({
            message: "employee not found with id " + req.params.id,
          });
        }
        res.redirect('/employees');
        // res.send(employee);
      })
      .catch((err) => {
        if (err.kind === "ObjectId") {
          return res.status(404).send({
            message: "employee not found with id " + req.params.id,
          });
        }
        return res.status(500).send({
          message: "Error updating employee with id " + req.params.id,
        });
      });
  }
};

exports.delete = function (req, res) {
  Employee.findByIdAndRemove(req.params.id, function (err, employee) {
    if (err) res.send(err);
    res.redirect('/employees');
    // res.json({ error: false, message: "Employee successfully deleted" });
  });
};
