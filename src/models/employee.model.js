'user strict';
var dbConn = require('./../../config/db.config');

// //Employee object create
// var Employee = function(employee){
//     this.first_name     = employee.first_name;
//     this.last_name      = employee.last_name;
//     this.phone          = employee.phone;
//     this.birthdate      = employee.birthdate;
// };

// Employee.findAll = function (result) {
//     // dbConn.query("Select * from employee", function (err, res) {
//     //     if(err) {
//     //         console.log("error: ", err);
//     //         result(null, err);
//     //     }
//     //     else{
//     //         console.log('employees : ', res);  
//     //         result(null, res);
//     //     }
//     // });   
//     dbConn.collection("employee").find({}).toArray(function(err, result) {
//         if (err) throw err;
//         console.log('employee', employee);
//         result(null, employee);

//         db.close();
//       });
// };

// Employee.create = function (newEmp, result) {    
//     dbConn.query("INSERT INTO employee set ?", newEmp, function (err, res) {
//         if(err) {
//             console.log("error: ", err);
//             result(err, null);
//         }
//         else{
//             console.log(res.insertId);
//             result(null, res.insertId);
//         }
//     });           
// };

// Employee.findById = function (id, result) {
//     dbConn.query("Select * from employee where id = ? ", id, function (err, res) {             
//         if(err) {
//             console.log("error: ", err);
//             result(err, null);
//         }
//         else{
//             result(null, res);
//         }
//     });   
// };

// Employee.update = function(id, employee, result){
//   dbConn.query("UPDATE employee SET first_name=?,last_name=?,phone=?, birthdate=? WHERE id = ?", [employee.first_name,employee.last_name,employee.phone,employee.birthdate, id], function (err, res) {
//         if(err) {
//             console.log("error: ", err);
//             result(null, err);
//         }else{   
//             result(null, res);
//         }
//     }); 
// };

// Employee.delete = function(id, result){
//      dbConn.query("DELETE FROM employee WHERE id = ?", [id], function (err, res) {
//         if(err) {
//             console.log("error: ", err);
//             result(null, err);
//         }
//         else{
//             result(null, res);
//         }
//     }); 
// };


// module.exports= Employee;


const mongoose = require('mongoose');

const EmployeeSchema = mongoose.Schema({
    firstname: String,
    lastname: String,
    phone: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Employee', EmployeeSchema);