const express = require('express');
const bodyParser = require('body-parser')
var path = require('path');

// create express app
const app = express();

// Setup server port
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }))

const index = require("./src/routes/index");
const employees = require("./src/routes/employee.routes");

app.use('/', employees);
app.use('/employees', employees);
app.engine('html', require('ejs').renderFile);

// about page
app.get('/about', function(req, res) {
  res.render('pages/about');
});

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Require employee routes
const employeeRoutes = require('./src/routes/employee.routes')
// Configuring the database
const dbConfig = require('./config/db.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
	useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// using as middleware
app.use('/api/v1/employees', employeeRoutes)

// listen for requests
// app.listen(port, () => {
//   console.log(`Server is listening on port ${port}`);
// });

var server = app.listen(3000, function () {  
  var host = server.address().address;  
  var port = server.address().port;  
  // res.status(err.status || 500);
  // res.render('error');
  console.log('Example app listening at http://%s:%s', host, port);  
});  